<h1>GSoC 2019 Kotlin 1.3.30 packaging</h1>  
I am maintating a blog [here](https://java-team.pages.debian.net/gsoc-kotlin-blog/).<br/>
I ll try to keep that updated. Please read that blog for progress.

<h2>Command to build project</h2>
./gradlew -Pteamcity=true dist

Source of this package is [here](https://github.com/JetBrains/kotlin/tree/1.3.30).
<h2>Enviornment Variables to be set</h2>
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"<br />
export JDK_16="/usr/lib/jvm/java-8-openjdk-amd64"<br />
export JDK_17="/usr/lib/jvm/java-8-openjdk-amd64"<br />
export JDK_18="/usr/lib/jvm/java-8-openjdk-amd64"<br />
export JDK_9="/usr/lib/jvm/java-11-openjdk-amd64"<br />
<br />

if you get the error ````Could not create service of type ScriptPluginFactory using BuildScopeServices.createScriptPluginFactory()```` 
then setting JAVA_HOME to jdk-8 fixes it.  

<h2>Version of Intellij-Core</h2>
The version of intellij-core needed seems to be [183.5153](https://github.com/JetBrains/intellij-community/tree/183.5153)

<h3>Important [Link](https://teamcity.jetbrains.com/project.html?projectId=Kotlin&tab=projectOverview) to check bootstraps and compilers</h3>

<h2>Notes of Changes and Descision I made</h2>
<h3>Converting kts to groovy in all build scripts</h3>
I am trying to make intermediatory mehtods between upstream methods in buildsrc 
written kotlin and build.gradle files I am translating to. I have done this so 
that any other script which is subtly using these methods wont get broken. 
  
while converting files in buildSrc and making them build without kotlin-dsl plugin 
I renamed a file files which I thought were not included in the dist path; these 
files have a _ignore extension in their names.
  
<h2>Converting to Groovy - The End</h2>  
All the build files in the dist path have been converted to groovy and the dependency 
on kotlin-dsl has been removed. The dist folder of the build project was copied 
to another fresh kotlin-1.3.30 project and had "jvmCompilerTest" run. It was observed 
that "jvmCompilerTest" ran similar to that of an unaltered project's dist. So we 
can conclude that translation to Groovy was indeed successfull. Also the test log 
of "distTest", which contains "jvmCompilerTest", of the unaltered project was posted 
in a file named UnalteredDistTest.txt .

<h2>Packaging Depenencies</h2>  
I have added a folder ./debian/maven-repo-cache so that we may store relocated protobuf 
artifact in there; please note that maven.rules will not change how artifacts in that 
folder are being resolved.

<h2>How to build; August 02 2019</h2>  
cd to the root of the project and run `debian/pseudoBootstrap bootstrap` the very first 
time to setup the bootstraps. Note that this is a temporary alternative till we pacakge 
the bootstrap and intellij-sdk/build.<br/>
Next run `debuild -b -rfakeroot -us -uc` and all the need packages are already up or in 
new Queue; their sources can be found [here](https://java-team.pages.debian.net/gsoc-kotlin-blog/2019/07/07/packaging-dependencies-part-1-and-plan-on-how-to/).</br>
Note: you need run `debian/pseudoBootstrap bootstrap` only once; i.e every other time you build the project just invoking debuild will work.